import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Planet, SwitchModes } from '@worktile/planet';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'portal-web';

  // get loadingDone() {
  //   return this.planet.loadingDone;
  // }

  constructor(private planet: Planet,
    private router: Router) {
  }

  ngOnInit() {
    this.planet.setOptions({
      switchMode: SwitchModes.default,
      errorHandler: error => {
        console.error(`Failed to load resource, error:`, error);
      }
    });

    this.planet.setPortalAppData({
      // appRootContext: this.appRootContext,
      // utilService: this.utilService,
      // apiService: this.apiService,
      // avatar1InjectorService: this.avatar1InjectorService,
      // observeSubjectService: this.observeSubjectService,
      // storageService: this.storageService,
      // authService: this.authService,
      // userAppService: this.userAppService,
      // httpApiHelperService: this.httpApiHelperService,
      foundationSvc: {}
    });

    this.planet.registerApps([
      {
        name: 'admin-web',
        hostParent: '#app-host-container',
        hostClass: 'thy-layout',
        routerPathPrefix: '/admin',
        selector: 'admin-app-root',
        preload: false,
        manifest: 'admin-web/manifest.json',
        scripts: [
          'admin-web/main.js',
        ],
        styles: [
          'admin-web/styles.css'
        ]
      },
      {
        name: 'sys_management_web',
        hostParent: '#app-host-container',
        routerPathPrefix: '/avatar2/module/sys-management',
        selector: 'system-management-root-container',
        preload: false,
        switchMode: SwitchModes.default,
        loadSerial: false,
        manifest: '/sys_management_proxy/sys_management_web/manifest.json',
        scripts: [
          '/sys_management_proxy/sys_management_web/main.js',
        ],
        styles: [
          '/sys_management_proxy/sys_management_web/sys.css'
        ],
      },
    ]);

    // start monitor route changes
    // get apps to active by current path
    // load static resources which contains javascript and css
    // bootstrap angular sub app module and show it
    this.planet.start();
  }

  gotoDomePage(event: any) {
    this.router.navigate([`/admin/dome-page`]);
  }

}
