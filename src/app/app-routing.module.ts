import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmptyComponent } from '@worktile/planet';

const routes: Routes = [
  {
    path: 'admin',
    component: EmptyComponent,
    data: {
      planet: 'Y'
  },
    children: [
      {
        path: '**',
        component: EmptyComponent,
        data: {
          planet: 'Y'
      },
      }
    ]
  },
  {
    path: 'avatar2/module/sys-management',
    component: EmptyComponent,
    data: {
        planet: 'Y'
    },
    children: [
        {
            path: '**',
            component: EmptyComponent,
            data: {
                planet: 'Y'
            },
            // canActivate: [AuthGuard]
        }
    ],
    // canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
